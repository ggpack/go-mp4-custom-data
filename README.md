# MP4 Custom Data

A dependency free lightweight module to get the user-defined fields (like title, artist, album, description, ...) of the MP4 file.

User-defined can carry information to display on a UI and then be directly stored in the media itself rather than being mapped in a database.


Inspired from https://github.com/abema/go-mp4
