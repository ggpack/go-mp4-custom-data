package customdata

import (
	"context"
	"encoding/binary"
	"io"
	"log"
	"strings"
	"unicode"
)

const largeHeaderSize = 12
const smallHeaderSize = 8

type ctxKey int

const (
	keyPath      ctxKey = iota
	keyUnderMeta        // Under this box, the header size is encoded on 12 bits
	keyUnderIlst        // Under this box, the header size is encoded back on 8 bits
)

// In some circonstances the heder size varies
func headerSize(hasLargeHeader bool) int {
	if hasLargeHeader {
		return largeHeaderSize
	} else {
		return smallHeaderSize
	}
}

// Removes any non printable rune of a string, some fields are prefilled with these
func filterUnicode(dirty rune) rune {
	if unicode.IsGraphic(dirty) {
		return dirty
	}
	return -1
}

// Gets the content of a `data` sub field in a box or empty string if nothing found
func ReadBoxData(file io.Reader, size int64, hasLargeHeader bool) (string, error) {

	data := make([]byte, size-int64(headerSize(hasLargeHeader)))
	_, err := file.Read(data)
	if err == nil {
		return strings.Map(filterUnicode, string(data)), nil
	} else {
		return "", err
	}
}

// Get its type and size
func ReadBoxHeader(file io.Reader, hasLargeHeader bool) (BoxType, int64, error) {

	header := make([]byte, headerSize(hasLargeHeader))
	_, err := file.Read(header)

	if err == nil {
		// Just ignore the first bytes
		if hasLargeHeader {
			header = header[largeHeaderSize-smallHeaderSize:]
		}

		return BoxType{header[4], header[5], header[6], header[7]}, int64(binary.BigEndian.Uint32(header)), nil
	} else {
		return DefaultBox, 0, err
	}
}

// Go deep through a sequence of boxes to get a value
func WalkBoxes(file io.ReadSeeker, ctx context.Context) (string, error) {
	path := ctx.Value(keyPath).(BoxPath)

	underMeta, okM := ctx.Value(keyUnderMeta).(bool)
	underIlst, okI := ctx.Value(keyUnderIlst).(bool)
	hasLargeHeader := (okM && underMeta) && !(okI && underIlst)

	for {
		currType, size, err := ReadBoxHeader(file, hasLargeHeader)
		if err != nil {
			log.Print("Read header error: ", err.Error())
			return "", err
		}

		if path[0] == currType {
			// Found an interesting box: go deeper
			if len(path) > 1 {

				if currType == MetaBox {
					ctx = context.WithValue(ctx, keyUnderMeta, true)
				}
				if currType == IlstBox {
					ctx = context.WithValue(ctx, keyUnderIlst, true)
				}
				ctx = context.WithValue(ctx, keyPath, path[1:])

				return WalkBoxes(file, ctx)
			} else {
				return ReadBoxData(file, size, hasLargeHeader)
			}

		} else {
			// Fast forward to next box
			_, err := file.Seek(size-int64(headerSize(hasLargeHeader)), io.SeekCurrent)

			if err != nil {
				log.Print("Seek next error: ", err.Error())
				return "", err
			}
		}
	}
}

// Entrypoint to get the content of a field deep in the MP4 structure at a given sequence of boxes
func GetFieldContent(file io.ReadSeeker, path BoxPath) (string, error) {
	file.Seek(0, io.SeekStart) // Rewind
	ctx := context.Background()
	ctx = context.WithValue(ctx, keyPath, path)
	return WalkBoxes(file, ctx)
}
