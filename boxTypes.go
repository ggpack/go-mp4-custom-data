package customdata

type BoxType [4]byte
type BoxPath []BoxType

var MoovBox = BoxType{'m', 'o', 'o', 'v'}
var UdtaBox = BoxType{'u', 'd', 't', 'a'}
var MetaBox = BoxType{'m', 'e', 't', 'a'}
var IlstBox = BoxType{'i', 'l', 's', 't'}
var CmtBox = BoxType{0xa9, 'c', 'm', 't'}
var GenBox = BoxType{0xa9, 'g', 'e', 'n'}
var NamBox = BoxType{0xa9, 'n', 'a', 'm'}
var DataBox = BoxType{'d', 'a', 't', 'a'}
var DefaultBox = BoxType{0, 0, 0, 0}

func (this BoxType) String() string {
	return string(this[:])
}
