module example

go 1.22

replace gitlab.com/ggpack/go-mp4-custom-data v0.0.0 => ../

require gitlab.com/ggpack/go-mp4-custom-data v0.0.0
