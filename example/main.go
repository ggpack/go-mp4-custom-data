package main

import (
	"log"
	"os"

	mp4 "gitlab.com/ggpack/go-mp4-custom-data"
)

func main() {
	log.Print("Logging via the standard package")

	// Edit it in windows or VLC (Tools / Media Information)
	file, err := os.Open("sample.mp4")
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	artist, _ := mp4.GetFieldContent(file, mp4.BoxPath{mp4.MoovBox, mp4.UdtaBox, mp4.MetaBox, mp4.IlstBox, mp4.BoxType{0xa9, 'A', 'R', 'T'}, mp4.DataBox})
	name, _ := mp4.GetFieldContent(file, mp4.BoxPath{mp4.MoovBox, mp4.UdtaBox, mp4.MetaBox, mp4.IlstBox, mp4.NamBox, mp4.DataBox})
	genre, _ := mp4.GetFieldContent(file, mp4.BoxPath{mp4.MoovBox, mp4.UdtaBox, mp4.MetaBox, mp4.IlstBox, mp4.GenBox, mp4.DataBox})

	log.Printf("The artist: %s, the name: %s, the genre: %s", artist, name, genre)
}
